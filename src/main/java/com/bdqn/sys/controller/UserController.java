package com.bdqn.sys.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Yang
 * @since 2023-12-12
 */
@Controller
@RequestMapping("/sys/user")
public class UserController {

}

