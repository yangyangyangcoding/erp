package com.bdqn.sys.service;

import com.bdqn.sys.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Yang
 * @since 2023-12-12
 */
public interface UserService extends IService<User> {

    User findUserByName(String userName) throws Exception;

}
