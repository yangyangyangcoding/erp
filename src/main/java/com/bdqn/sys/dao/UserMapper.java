package com.bdqn.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bdqn.sys.entity.User;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Yang
 * @since 2023-12-12
 */
//@Mapper  启动类通过 @MapperScan("com.bdqn.*.dao")
public interface UserMapper extends BaseMapper<User> {

}
